﻿using Microsoft.AspNetCore.Mvc;
using System.Text.Encodings.Web;
using System.Collections.Generic;
using System;

namespace ModelPrac.Controllers
{
    public class HomeController : Controller
    {
        public IList<string> titles = new List<string>
        {
            "Hello World!",
            "This is a title",
            "Is it random?"
        };

        public Random rnd = new Random();

        public IActionResult Index()
        {
            ViewBag.Title = titles[rnd.Next(0, titles.Count)];
            ViewBag.h1 = "This is the h1 Tag";
            ViewBag.h2 = "This is the h2 Tag";
            ViewBag.h3 = "This is the h3 Tag";
            ViewBag.h4 = "This is the h4 Tag";
            ViewBag.h5 = "This is the h5 Tag";
            ViewBag.h6 = "This is the h6 Tag";
            return View();
        }
    }
}
